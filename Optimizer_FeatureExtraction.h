#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>

/////////////////////////////////////////////////////
// #define COMMENT_OUT_ALL_PRINTS

#define ONLY_ANNAS_PART_REMAINS

#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)


//#define PRINT_DEBUGGING_INFO
#define eps 1.0E-6
#define fLarge 1.0E+8 //10

#define nDim 20 //100 
#define nNumOfFeasHighSeparability 10 // < nDim

#define nDimSelecMax 5

//#define fEfficBestSeparOfOneFeaAcceptMax (-1.55) //the min at the perfect separation is (-2.0) 
#define fEfficBestSeparOfOneFeaAcceptMax (-1.55) //the min at the perfect separation is (-2.0) 

#define nNumVecTrain_1st 10 

#define nNumVecTrain_2nd (nNumVecTrain_1st) //1000 //100

#define nProdTrain_1st (nDim*nNumVecTrain_1st)

#define nProdTrain_2nd (nDim*nNumVecTrain_2nd)

#define nNumIterForDataGenerMax (nNumVecTrain_1st*50)

#define nNumDistiguishFeasForInit 2
/////////////////////////////////////////////////

#define fThreshold_HighSeparability 0.5
#define fThreshold_LowSeparability_1 0.3
#define fThreshold_LowSeparability_2 0.7
/////////////////////////////////////////////////
#define fBorderBel 0.0
#define fBorderAbove 1.0

#define nNumIterMaxForFindingBestSeparByRatioOfOneFea 10 //100
#define nNumIterMaxForFindingBestSeparByDiffOfOneFea (nNumIterMaxForFindingBestSeparByRatioOfOneFea)

////////////////////////////////////
#define fC_Const 6.18034E-01 //( (-1.0 + sqrt(5.0) )/2.0 ) 
#define fR_Const 3.81966E-01 //(1.0 - fC_Const)

#define fEfficOfArr1stRequired 0.9

const float fPrecisionOf_G_Const_Search = (float)(0.01); // 0.005
